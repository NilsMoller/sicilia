<div class="inPageMenuLinks">
    <ul class="inPageMenuLinks">
        <li><a href = "#" onclick="loadInfo(this, 'Menu_Starters.php')">Starters</a><br></li>
        <li><a href = "#" onclick="loadInfo(this, 'Menu_Salads.php')">Salads</a><br></li>
        <li><a href = "#" onclick="loadInfo(this, 'Menu_MainDishes.php')">Main dishes</a><br></li>
        <li><a href = "#" onclick="loadInfo(this, 'Menu_Specialties.php')">Specialties</a><br></li>
        <li><a href = "#" onclick="loadInfo(this, 'Menu_Desserts.php')">Desserts</a><br></li>
        <li><a href = "#" onclick="loadInfo(this, 'Menu_Sauces.php')">Sauces</a><br></li>
        <li><a href = "#" onclick="loadInfo(this, 'Menu_Beverages.php')">Beverages</a><br></li>
    </ul>
</div>

<div align="middle" id="menuDessertsDiv">
    <h1>Menu - desserts</h1>
    <table class="menuTables" id="menuDessertsTable" border="white">
        <tr>
            <th>
                <img src="pictures/tiramisu.jpg" alt="Tiramisu picture" width="100" height="100">
                <p><i>Tiramisu</i></p>200 gr. €7.00
                <button type="button" onclick="AddItemToCart('Tiramisu','1','7.00')"><img src="pictures\cart.png" width="25" height="25"> </button>
            </th>
            <th>
                <img src="pictures/crostata.jpg" alt="Crostata picture" width="100" height="100">
                <p>Crostata</p>150 gr. €5.60
                <button type="button" onclick="AddItemToCart('Crostata','1','5.60')"><img src="pictures\cart.png" width="25" height="25"> </button>
            </th>
        </tr>
        <tr>
            <th>
                <img src="pictures/cannoli.jpg" alt="Cannoli picture" width="100" height="100">
                <p>Cannoli</p>200 gr. €6.12
                <button type="button" onclick="AddItemToCart('Cannoli','1','6.12')"><img src="pictures\cart.png" width="25" height="25"> </button>
            </th>
            <th>
                <img src="pictures/panna-cotta.jpg" alt="Panna cotta picture" width="100" height="100">
                <p>Panna cotta</p>150 gr. €5.35
                <button type="button" onclick="AddItemToCart('Panna cotta','1','5.35')"><img src="pictures\cart.png" width="25" height="25"> </button>
            </th>
        </tr>
        <tr>
            <th>
                <img src="pictures/brownie.jpg" alt="Chocolate brownie picture" width="100" height="100">
                <p>Chocolate brownie</p>120 gr. €5.89
                <button type="button" onclick="AddItemToCart('Chocolate brownie','1','5.89')"><img src="pictures\cart.png" width="25" height="25"> </button>
            </th>
            <th>
                <img src="pictures/affogato.jpeg" alt="Affogato picture" width="100" height="100">
                <p>Affogato</p>250gr. €6.25
                <button type="button" onclick="AddItemToCart('Affogato','1','6.25')"><img src="pictures\cart.png" width="25" height="25"> </button>
            </th>
        </tr>
    </table>
</div>