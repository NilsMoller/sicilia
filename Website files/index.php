<?php
    session_start();
?>
<!doctype html>
<html>
    <head>
        <meta charset="UTF-8">
        <link rel="stylesheet" type="text/css" href="style.css">
        <title>Sicilia - Home</title>
    </head>
    <body>

        <div class="navbar">
            <a href="#" onclick="loadInfo(this, 'HomePage.php')" class="nav_btns active">Sicilia</a>
            <a href="#" onclick="loadInfo(this, 'OurStory.php')" class="nav_btns">Our story</a>
            <div class="menu">
                <button class="nav_btns dropdown_btn">Menu</button>
                <div class="menu_dropdown">
                    <a href = "#" class="nav_btns" onclick="loadInfo(this, 'Menu_Starters.php')">Starters</a>
                    <a href = "#" class="nav_btns" onclick="loadInfo(this, 'Menu_Salads.php')">Salads</a>
                    <a href = "#" class="nav_btns" onclick="loadInfo(this, 'Menu_MainDishes.php')">Main dishes</a>
                    <a href = "#" class="nav_btns" onclick="loadInfo(this, 'Menu_Specialties.php')">Specialties</a>
                    <a href = "#" class="nav_btns" onclick="loadInfo(this, 'Menu_Desserts.php')">Desserts</a>
                    <a href = "#" class="nav_btns" onclick="loadInfo(this, 'Menu_Sauces.php')">Sauces</a>
                    <a href = "#" class="nav_btns" onclick="loadInfo(this, 'Menu_Beverages.php')">Beverages</a>
                </div>
            </div>
            <div class="menu">
                <button class="nav_btns dropdown_btn">Events</button>
                <div class="menu_dropdown">
                    <a href = "#" class="nav_btns" id="plannedEventsButton" onclick="loadInfo(this, 'Events_Planned.php')">Planned Events</a>
                    <a href = "#" class="nav_btns" id="planYourEvent" onclick="loadInfo(this, 'Events_PlanYourEvents.php')">Plan your event</a>
                </div>
            </div>
            <div class="menu">
                <button class="nav_btns dropdown_btns">Contact us</button>
                <div class="menu_dropdown">
                    <a href = "#" class="nav_btns" onclick="loadInfo(this, 'ContactUs_Location.php')">Location</a>
                    <a href = "#" class="nav_btns" onclick="loadInfo(this, 'ContactUs_ContactForm.php')">Contact form</a>
                </div>
            </div>
            <a href = "#" class="nav_btns" onclick="loadInfo(this, 'Cart.php');"><img src = "pictures/icon_payment.png" id="cartIcon"></a>
        </div>
        <div id="loginDiv">
            <?php
                if (isset($_SESSION['Email']))
                {
                    echo '<form action="PHP/logoutAction.php" method="post">
                          <input type="submit" name="submit" value="Log out" class="nav_btns">
                          </form>';
                }
                else
                {
                    echo '<form id="loginForm" action="PHP/loginAction.php" method="post">
                          <input type="text" name="loginEmail" placeholder="Email" class="inputBoxesLogin">
                          <input type="password" name="password" placeholder="Password" class="inputBoxesLogin">
                          <input type="submit" name="submit" class="nav_btns" value="Log in">
                          <a href="#" class="nav_btns" onclick="loadInfo(this, \'signup.php\')" id="loadSignupButton">Sign up</a>
                          </form>
                          ';
                }
            ?>
        </div>

        <div id="pageContent">

        </div>

        <div id="footer">
            <p>@ 2018 SICILIA</p>
        </div>

        <script src="Scripts/pageLoading.js" type="text/javascript"></script>
        <script src="Scripts/cart.js"></script>
        <!--<script>document.onload(AddItemToCart('Aperol Spritz','1','20.50'));</script>-->
    </body>
</html>