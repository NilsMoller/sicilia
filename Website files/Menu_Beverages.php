<div class="inPageMenuLinks">
    <ul class="inPageMenuLinks">
        <li><a href = "#" onclick="loadInfo(this, 'Menu_Starters.php')">Starters</a><br></li>
        <li><a href = "#" onclick="loadInfo(this, 'Menu_Salads.php')">Salads</a><br></li>
        <li><a href = "#" onclick="loadInfo(this, 'Menu_MainDishes.php')">Main dishes</a><br></li>
        <li><a href = "#" onclick="loadInfo(this, 'Menu_Specialties.php')">Specialties</a><br></li>
        <li><a href = "#" onclick="loadInfo(this, 'Menu_Desserts.php')">Desserts</a><br></li>
        <li><a href = "#" onclick="loadInfo(this, 'Menu_Sauces.php')">Sauces</a><br></li>
        <li><a href = "#" onclick="loadInfo(this, 'Menu_Beverages.php')">Beverages</a><br></li>
    </ul>
</div>

<div  id="menuBeveragesDiv" class="menuDivs" align="middle">
    <h1>Menu - beverages</h1>
    <table class="menuTables" id="menuBeveragesTable" border="white">
        <tr>
            <th>
                <img src="pictures\aperol.jpg" alt="Aperol Spritz picture" width="100" height="100">
                <p><i>Aperol Spritz</i></p>A wine-based cocktail.<br><br>700 ml €20.50
                <button type="button" onclick="AddItemToCart('Aperol Spritz','1','20.50')"><img src="pictures\cart.png" width="25" height="25"> </button>
            </th>
            <th>
                <img src="pictures\campari.jpg" alt="Campari picture" width="100" height="100">
                <p><i>Campari</i></p>Italian liquor (23%).<br><br>350 ml  €27.25
                <button type="button" onclick="AddItemToCart('Campari', '1', '27.25')"><img src="pictures\cart.png" width="25" height="25"> </button>
            </th>
        </tr>
        <tr>
            <th>
                <img src="pictures/amaretto_disaronno.jpg" alt="Amaretto disaronno picture" width="100" height="100">
                <p><i>Amaretto Disaronno</i></p>Sweet almond-flavoured<br>Italian liquor.<br><br>500 ml €26.00
                <button type="button" onclick="AddItemToCart('Amaretto Disaronno', '1', '26.00')"><img src="pictures\cart.png" width="25" height="25"> </button>
            </th>
            <th>
                <img src="pictures\Amaro-Montenegro.png" alt="Amaro Montenegro picture" width="100" height="100">
                <p><i>Amaro Montenegro</i></p>Traditional liquor,<br>destilled in Bologna, Italy.<br><br>400ml  €45.50
                <button type="button" onclick="AddItemToCart('Amaro Montenegro', '1', '45.50')"><img src="pictures\cart.png" width="25" height="25"> </button>
            </th>
        </tr>
        <tr>
            <th>
                <img src="pictures/sparkling-raspberry-lemonade-srgb..jpg" alt="Raspberry lemonade picture" width="100" height="100">
                <p><i>Raspberry lemonade</i></p>500 ml €5.80
                <button type="button" onclick="AddItemToCart('Raspberry lemonade', '1', '5.80')"><img src="pictures\cart.png" width="25" height="25"> </button>
            </th>
            <th>
                <img src="pictures/Basil-Lemonade.jpg" alt="Homemade lemonade picture" width="100" height="100">
                <p><i>Homemade lemonade</i></p>500 ml €5.25
                <button type="button" onclick="AddItemToCart('Homemade lemonade', '1', '5.25')"><img src="pictures\cart.png" width="25" height="25">
                </button>
            </th>
        </tr>
    </table>
</div>