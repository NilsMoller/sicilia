<!doctype html>
<html>
<head>
    <title>Logging out</title>
    <link rel="stylesheet" type="text/css" href="../style.css">
</head>
<body>
<?php
    if (isset($_POST['submit'])){
        session_start();
        session_unset();
        session_destroy();
        echo "<script>
            alert('Logged out');
            window.location.href='../index.php';
            </script>";
    }
?>
</body>
</html>