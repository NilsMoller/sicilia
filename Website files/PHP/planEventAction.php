<!doctype html>
<html>
<head>
    <title>Planned events</title>
    <link rel="stylesheet" type="text/css" href="../style.css">
</head>
<body>
<?php
    if(isset($_POST['btnSubmitEvent'])){
        try{
            $conn = new PDO('mysql:host=studmysql01.fhict.local; dbname=dbi392163', 'dbi392163', 'Touqmoller1');
            $Descripton = $_POST['inputDescriptionOfEvent'];
            $formDate = $_POST['inputDateOfEvent'];
            $formTime = $_POST['inputTimeOfEvent'];
            $DateOfEvent = date("Y-m-d",strtotime($formDate));
            $TimeOfEvent = date("H:i:s",strtotime($formTime));
            if (isset($_SESSION['Email'])){
                $Email = $_SESSION['Email'];
                $PhoneNumber = $_SESSION['PhoneNumber'];
            }
            else{
                $Email = $_POST['inputEmailOfPlanningCustomer'];
                $PhoneNumber = $_POST['inputTelOfPlanningCustomer'];
            }
            $sqlInsertStatement = "INSERT INTO events (Description, dateOfEvent, timeOfEvent, PhoneNumber, Email) VALUES(:description, :dateofevent, :timeofevent, :phonenumber, :email);";
            $preparedSQL = $conn->prepare($sqlInsertStatement);
            if (!isset($_SESSION['Email'])){
                $preparedSQL->execute(['description' => $Descripton, 'dateofevent' => $formDate, 'timeofevent' => $TimeOfEvent, 'phonenumber' => $PhoneNumber, 'email' => $Email]);
            }
            else if(isset($_SESSION['Email'])){
                $preparedSQL->execute(['description' => $Descripton, 'dateofevent' => $DateOfEvent, 'timeofevent' => $TimeOfEvent, 'phonenumber' => $_SESSION['PhoneNumber'], 'email' => $_SESSION['Email']]);
            }
            echo "<script>
            alert('Successfully added event');
            </script>";
        }
        catch (PDOException $e){
            echo "<script>
            alert('Database error');
            </script>";
        }
        catch (InvalidArgumentException $e){
            echo "<script>
            alert('Unexpected input type');
            </script>";
        }
        finally{
            $conn = null;
            echo "<script>
            window.location.href='../index.php';
            </script>";
        }
    }
?>
</html>