<!doctype html>
<html>
<head>
    <title>Planned events</title>
    <link rel="stylesheet" type="text/css" href="../style.css">
</head>
<body>
<div id="pageContent">
    <h1>Planned events</h1>
    <?php
        if (isset($_POST['submit'])) {
            session_start();
            $timezone = date_default_timezone_get();
            $currentDate = date('Y-m-d');
            $dateToShow = date("Y-m-d", strtotime($_POST['dateToShow']));
            if ($dateToShow > $currentDate) {
                try {
                    $conn = new PDO('mysql:host=studmysql01.fhict.local; dbname=dbi392163', 'dbi392163', 'Touqmoller1');
                    $sqlGetStatement = "SELECT Description, dateOfEvent, timeOfEvent FROM events WHERE dateOfEvent BETWEEN DATE_SUB(SYSDATE(), INTERVAL 1 DAY) AND :dateToShow ORDER BY dateOfEvent ASC ;";
                    $stmt = $conn->prepare($sqlGetStatement);
                    $result = $stmt->execute(['dateToShow' => $dateToShow]);
                    if ($stmt->rowCount() > 0) {
                        $table = '<table id="plannedEventsTable" border="white">';
                        $table .= '<tr>';
                        $counter = 0;
                        while ($row = $stmt->fetch(PDO::FETCH_ASSOC)) {
                            $table .= '<td>';
                            $table .= ($row['Description'] . "<br>" . $row['dateOfEvent'] . '<br>' . $row['timeOfEvent'] . '<br>');
                            $table .= '</td>';
                            $counter++;
                            if ($counter == 4) {
                                $table .= '</tr>';
                                $counter = 0;
                            }
                        }
                        $table .= "</table>";
                        $_SESSION['plannedTable'] = $table;
                        //echo "<script>document.getElementById('pageContent').innerHTML = " . $table . ";</script>";
                        echo $table;
                        echo '<!doctype html><html><button type="button" id="goHomeButton" onclick="window.location.href=\'../index.php\';">Go to home</button> </html>';
                    } else {
                        echo "<script>
                    alert('No events are planned until that date.');
                    window.location.href='../index.php';
                    </script>";
                    }
                } catch (PDOException $e) {
                    echo "<script>
                alert('Database error');
                window.location.href='../index.php';
                </script>";
                } finally {
                    $conn = null;
                }
            } else {
                echo "<script>
            alert('Cannot show past events');
            window.location.href='../index.php';
            </script>";
            }
        }
    ?>
</div>
<div id="footer">
    <p>@ 2018 SICILIA</p>
</div>
</body>
</html>