<!doctype html>
<html>
<head>
    <title>Creating account</title>
    <link rel="stylesheet" type="text/css" href="../style.css">
</head>
<body>
<?php
    //ADD CHECK FOR EXISTING EMAIL
    if(isset($_POST['submit'])){
        try{
            $conn = new PDO('mysql:host=studmysql01.fhict.local; dbname=dbi392163', 'dbi392163', 'Touqmoller1');
            $Email = $_POST['email'];
            $FirstName = $_POST['firstName'];
            $LastName = $_POST['lastName'];
            $Password = $_POST['password'];
            $phone = $_POST['PhoneNumber'];
            $repeatedPassword = $_POST['repeatpassword'];
            $preparedEmailCheck = $conn->prepare("SELECT Email, PhoneNumber FROM logindata");
            $preparedEmailCheck->execute();
            if ($preparedEmailCheck->rowCount() > 0){ //checks with the db
                while ($row = $preparedEmailCheck->fetch(PDO::FETCH_ASSOC)){
                    if ($Email == $row['Email']){
                        echo "<script>
                        alert('An account with that email already exists!');
                        </script>";
                        return;
                    }
                    elseif ($phone == $row['PhoneNumber']){
                        echo "<script>
                        alert('An account with that phone number already exists!');
                        </script>";
                        return;
                    }
                }
            }
            //check empty fields
            if (($Email == '') || ($FirstName == '') || ($LastName == '') || ($phone == '') || ($Password == '')){
                echo "<script>
                alert('Please fill in all fields');
                </script>";
            }
            elseif ((strlen($Password)) < 5){
                echo "<script>
                alert('Password must be longer than 5 characters.');
                </script>";
            }
            elseif ($repeatedPassword != $Password){
                echo "<script>
                alert('Passwords must be the same');
                </script>";
            }
            //if everything is fine do the following
            else{
                $sqlInsertStatement = "INSERT INTO logindata (Email, FirstName, LastName, Password, PhoneNumber) VALUES(:Email, :FirstName, :LastName, :Password, :PhoneNumber);";
                $preparedSQL = $conn->prepare($sqlInsertStatement);
                $preparedSQL->execute(['Email' => $Email, 'FirstName' => $FirstName, 'LastName' => $LastName, 'Password' => $Password, 'PhoneNumber' => $phone]);
                echo "<script>
                alert('Thank you for joining us!');
                </script>";
            }
        }
        catch (PDOException $e){
            echo "<script>
            alert('Database error');
            </script>";
        }
        finally{
            $conn = null;
            echo "<script>
            window.location.href='../index.php';
            </script>";
        }
    }
?>
</body>
</html>